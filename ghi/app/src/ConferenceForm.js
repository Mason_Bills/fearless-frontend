import React, { useState, useEffect } from 'react';
import {ReactComponent as Logo } from './logo_1.svg';
function CreateConferenceForm() {
  const [locations, setLocations] = useState([]);
  const [formInputs, setFormInputs] = useState({
    name: "",
    starts: "",
    ends: "",
    description: "",
    max_presentations: "",
    max_attendees: "",
    location: "",
  });

  useEffect(() => {
    fetch('http://localhost:8000/api/locations/')
      .then(res => res.json())
      .then(data => setLocations(data.locations));
  }, []);

  const handleChange = (e) => {
    setFormInputs({
      ...formInputs,
      [e.target.name]: e.target.value
    });
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    const response = await fetch('http://localhost:8000/api/conferences/', {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(formInputs)
    });
    if (response.ok) {
      const newConference = await response.json();
      console.log(newConference);
      setFormInputs({
        name: "",
        starts: "",
        ends: "",
        description: "",
        max_presentations: "",
        max_attendees: "",
        location: "",
      });
    }
  }

  return (
//     <form onSubmit={handleSubmit} id='create-conference-form'>
//       <label>Name:</label>
//       <input type='text' name='name' value={formInputs.name} onChange={handleChange} required />

//       <label>Starts:</label>
//       <input type='datetime-local' name='starts' value={formInputs.starts} onChange={handleChange} required />

//       <label>Ends:</label>
//       <input type='datetime-local' name='ends' value={formInputs.ends} onChange={handleChange} required />

//       <label>Description:</label>
//       <textarea name='description' value={formInputs.description} onChange={handleChange} required />

//       <label>Max presentations:</label>
//       <input type='number' name='max_presentations' value={formInputs.max_presentations} onChange={handleChange} required />

//       <label>Max attendees:</label>
//       <input type='number' name='max_attendees' value={formInputs.max_attendees} onChange={handleChange} required />

//       <label>Location:</label>
//       <select name='location' value={formInputs.location} onChange={handleChange} required>
//         {locations.map(location => (
//           <option key={location.id} value={location.id}>
//             {location.name}
//           </option>
//         ))}
//       </select>

//       <input type='submit' value='Create conference' />
//     </form>
//   );
// }
<>
<div>
<Logo style={{width: '200px', height: '200px'}}/>
{/* <img src="/logo.svg" alt="Company Logo" /> */}
{/* rest of your component */}
</div>
<form onSubmit={handleSubmit} id='create-conference-form' className="container mt-4">
<div className="mb-3">
  <label className="form-label">Name:</label>
  <input type='text' name='name' value={formInputs.name} onChange={handleChange} className="form-control" required />
</div>

<div className="mb-3">
  <label className="form-label">Starts:</label>
  <input type='datetime-local' name='starts' value={formInputs.starts} onChange={handleChange} className="form-control" required />
</div>

<div className="mb-3">
  <label className="form-label">Ends:</label>
  <input type='datetime-local' name='ends' value={formInputs.ends} onChange={handleChange} className="form-control" required />
</div>

<div className="mb-3">
  <label className="form-label">Description:</label>
  <textarea name='description' value={formInputs.description} onChange={handleChange} className="form-control" required />
</div>

<div className="mb-3">
  <label className="form-label">Max presentations:</label>
  <input type='number' name='max_presentations' value={formInputs.max_presentations} onChange={handleChange} className="form-control" required />
</div>

<div className="mb-3">
  <label className="form-label">Max attendees:</label>
  <input type='number' name='max_attendees' value={formInputs.max_attendees} onChange={handleChange} className="form-control" required />
</div>

<div className="mb-3">
  <label className="form-label">Location:</label>
  <select name='location' value={formInputs.location} onChange={handleChange} className="form-select" required>
    {locations.map(location => (
      <option key={location.id} value={location.id}>
        {location.name}
      </option>
    ))}
  </select>
</div>

<div className="mb-3">
  <input type='submit' value='Create conference' className="btn btn-primary" />
</div>
</form>
</>
  );    }
export default CreateConferenceForm;
