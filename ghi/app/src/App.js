// import React, { useEffect, useState } from 'react';
// import Nav from './Nav';
// import AttendeesList from './AttendeesList';
// import LocationForm from './LocationForm';
// import AttendeesForm from './AttendeeForm'; // fixed typo
// import ConferenceForm from './ConferenceForm';

// function App() {
//   const [attendees, setAttendees] = useState([]);
  
//   useEffect(() => {
//     const loadAttendees = async () => {
//       const response = await fetch('http://localhost:8001/api/attendees/');
//       if (response.ok) {
//         const data = await response.json();
//         setAttendees(data.attendees);
//       } else {
//         console.error(response);
//       }
//     };
    
//     loadAttendees();
//   }, []);

//   if (attendees === undefined) {
//     return null;
//   }

//   return (
//     <>
//       <Nav />
//       <div className="container">
//         <ConferenceForm />
//         <AttendeesForm />
//         <LocationForm />
//         <AttendeesList attendees={attendees} />
//       </div>
//     </>
//   );
// }

// export default App;

import React, { useEffect, useState } from 'react';
import { BrowserRouter as Route, Routes, BrowserRouter } from 'react-router-dom';
import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import AttendeesForm from './AttendeeForm'; 
import ConferenceForm from './ConferenceForm';

function App() {
  const [attendees, setAttendees] = useState([]);
  
  useEffect(() => {
    const loadAttendees = async () => {
      const response = await fetch('http://localhost:8001/api/attendees/');
      if (response.ok) {
        const data = await response.json();
        setAttendees(data.attendees);
      } else {
        console.error(response);
      }
    };
    
    loadAttendees();
  }, []);

  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/conference" element={<ConferenceForm />} />
          <Route path="/attendees" element={<AttendeesForm />} />
          <Route path="/location" element={<LocationForm />} />
          <Route path="/" element={<AttendeesList attendees={attendees} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;







