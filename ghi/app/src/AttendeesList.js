import React, { useEffect, useState } from 'react';
import Nav from './Nav';

function Attendees() {
  const [attendees, setAttendees] = useState(null);
  
  useEffect(() => {
    async function loadAttendees() {
      const response = await fetch('http://localhost:8001/api/attendees/');
      if (response.ok) {
        const data = await response.json();
        setAttendees(data.attendees);
        console.log(data);
      } else {
        console.error(response);
      }
    }
    loadAttendees();
  }, []); // This empty array means the effect will only run once, similar to componentDidMount

  if (attendees === null) {
    return <div>Loading...</div>; // Or some other placeholder
  } else {
    return (
        <>
        <Nav />
        <div className="container">
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Conference</th>
            </tr>
          </thead>
          <tbody>
            {attendees.map(attendee => {
              return (
                <tr key={attendee.href}>
                  <td>{ attendee.name }</td>
                  <td>{ attendee.conference }</td>
                </tr>
              );
            })}
          </tbody>
        </table>
        </div>
        </>
    );
  }
}

export default Attendees;