import React, { useState, useEffect } from 'react';
import { ReactComponent as Logo } from './logo_1.svg';

const AttendeesForm = () => {
  const [conferences, setConferences] = useState([]);
  const [loading, setLoading] = useState(true);
  const [formData, setFormData] = useState({
    name: '',
    conference: '',
    email: '',
  });
  const [successMessage, setSuccessMessage] = useState(false);

  useEffect(() => {
    const fetchData = async () => {
      const url = 'http://localhost:8000/api/conferences/';
      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setConferences(data.conferences);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData(prevState => ({ ...prevState, [name]: value }));
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const attendeeURL = 'http://localhost:8001/api/attendees/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };
    const response = await fetch(attendeeURL, fetchConfig);
    if (response.ok) {
      setFormData({ name: '', conference: '', email: '' });
      setSuccessMessage(true);
    }
  };

  if (loading) {
    return <div className="d-flex justify-content-center mb-3" id="loading-conference-spinner">
             <div className="spinner-grow text-secondary" role="status">
               <span className="visually-hidden">Loading...</span>
             </div>
           </div>;
  } else {
    return (
      <div className="container my-5">
        <div className="row">
          <div className="col col-sm-auto">
            <Logo width="300" className="bg-white rounded shadow d-block mx-auto mb-4" />
          </div>
          <div className="col">
            <div className="card shadow">
              <div className="card-body">
                <form id="create-attendee-form" onSubmit={handleSubmit}>
                  <h1 className="card-title">It's Conference Time!</h1>
                  <p className="mb-3">Please choose which conference you'd like to attend.</p>
                  <div className="mb-3">
                    <select onChange={handleChange} id="conference" name="conference" className="form-select" required>
                      <option value="">Choose a conference</option>
                      {conferences.map(conference => (
                        <option key={conference.href} value={conference.href}>{conference.name}</option>
                      ))}
                    </select>
                  </div>
                  <p className="mb-3">Now, tell us about yourself.</p>
                  <div className="row">
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input required placeholder="Your full name" type="text" id="name" name="name" className="form-control" onChange={handleChange} />
                        <label htmlFor="name">Your full name</label>
                      </div>
                    </div>
                    <div className="col">
                      <div className="form-floating mb-3">
                        <input required placeholder="Your email address" type="email" id="email" name="email" className="form-control" onChange={handleChange} />
                        <label htmlFor="email">Your email address</label>
                      </div>
                    </div>
                  </div>
                  <button className="btn btn-lg btn-primary">I'm going!</button>
                </form>
                {successMessage && (
                  <div id="success-message" className="alert alert-success mt-2">
                    Attendee created successfully!
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
};

export default AttendeesForm;
