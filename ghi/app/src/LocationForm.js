
import React, { useEffect, useState } from 'react';

const LocationForm = () => {
  const [states, setStates] = useState([]);
  const [formData, setFormData] = useState({
    name: '',
    room_count: '',
    city: '',
    state: '',
  });

  const handleChange = (event) => {
    const { name, value } = event.target;
    setFormData(prevState => ({ ...prevState, [name]: value }));
  }

  const handleSubmit = async (event) => {
    event.preventDefault();
  
    const data = {};
    data.room_count = formData.room_count;
    data.name = formData.name;
    data.city = formData.city;
    data.state = formData.state;
  
    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };
  
    // const response = await fetch(locationUrl, fetchConfig);
    // if (response.ok) {
    //   const newLocation = await response.json();
    //   console.log(newLocation);
    //   setName('');
    //   setRoomCount('');
    //   setCity('');
    //   setState('');
    // }
  }

  useEffect(() => {
    const fetchData = async () => {
      const url = 'http://localhost:8000/api/states/';
      const response = await fetch(url);

      if (response.ok) {
        const data = await response.json();
        setStates(data.states);
      }
    };

    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new location</h1>
          <form onSubmit={handleSubmit} id="create-location-form">
            <div className="form-floating mb-3">
              <input value={formData.name} onChange={handleChange} placeholder="Name" required type="text" name="name" className="form-control"/>
              <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChange} placeholder="Room count" required type="number" name="room_count" className="form-control"/>
              <label htmlFor="room_count">Room count</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleChange} placeholder="City" required type="text" name="city" className="form-control"/>
              <label htmlFor="city">City</label>
            </div>
            <div className="mb-3">
              <select onChange={handleChange} name="state" className='form-select'>
                <option value="">Select a state</option>  
                {states.map((state) => (
                  <option key={state.abbreviation} value={state.abbreviation}>
                    {state.name}
                  </option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default LocationForm;

