

function createCard(image) {
    return `<img src="${image}" class="card-img-top">`;
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';
    try {
      const response = await fetch(url);
      if (!response.ok) {
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        
        const conference = data.conferences[0];
        const nameTag = document.querySelector('.card-title');
        nameTag.innerHTML = conference.name;

        const detailUrl = `http://localhost:8000${conference.href}`;
        const detailResponse = await fetch(detailUrl);

        
        
        if (detailResponse.ok) {
            const details = await detailResponse.json();

            const description = details.conference.description;
            const descriptionTag = document.querySelector('.card-text');

            const image = details.conference.location.picture_url;
            // console.log(location)
            // console.log(image)
            // console.log(picture_url) Why does this line break the conference description?

            
            const html = createCard(image);
            const imageTag = document.querySelector('.card-img-top');


            imageTag.src = image
            descriptionTag.innerHTML = description
        }
      }
    } catch (e) {
      // Figure out what to do if an error is raised
    }
  });



  

