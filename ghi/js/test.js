// window.addEventListener('DOMContentLoaded', async () => {
    
//     const url = 'http://localhost:8000/api/conferences/'; 
//     //Gives access to the conferences object that has the attributes href and name

//     try {
//       const response = await fetch(url);
//       // goes and gets the data from the endpoint which is the object that has the attributes href and name
//       if (!response.ok) {
//         // checks the ok property of the response
        
//         // Figure out what to do when the response is bad
      
//         } else {
//         console.log(response)
//         const data = await response.json();
//         // formats the data retrieved from the response into json 
//         console.log(data)
//         const conference = data.conferences[0];
//         //accesses the first element of the array in the object conferences which is now a property of the object data
//         console.log(conference)
//         const nameTag = document.querySelector('.card-title');
//         //uses the querySelector method to select the element with the class card-title and set that to a variable called nameTag
//         nameTag.innerHTML = conference.name;
//         //calls the innerHTML property of the nameTag element with the value of the conference.name property
//         const detailUrl = `http://localhost:8000${conference.href}`;
//         // setting a new variable called detailUrl that is the url, the endpoint of the conferences's detail which contains all of these attributes: (see conference model)
//         const detailResponse = await fetch(detailUrl);
//         console.log(detailResponse)
//         // setting a new variable called detailResponse that is the response from the detailUrl endpoint
//         if (detailResponse.ok) {
//             // checks the ok property of detaiResponse
//           const details = await detailResponse.json();
//           console.log(details)
//           // sets new varibale details to the response from the detailUrl endpoint
//           const description = details.conference.description;
//           console.log(description)
//           // sets new variable description which accesses the properties of the model found at the endpoint 
//           const descriptionTag = document.querySelector('.card-text');
//           descriptionTag.innerHTML = description
//         }
//       }
//     } catch (e) {
//       // Figure out what to do if an error is raised
//     }
//   });

// in this code there are two objects. the first what i will label as "conference general" and the second one is "conference detail". The conference general has the name of the conference and the href of the conference detail. 

function mode(numbers) {
const counts = {};
// sets variable counts to an empty object
let maxCount = 0;
// sets variable maxCount to 0
let mode = null;
// sets variable mode to null
for (let number of numbers) {
  // sets up for loop to go through each number
  counts[number] = (counts[number] || 0) + 1;
  //
  if (counts[number] > maxCount) {
      maxCount = counts[number];
      mode = number;
  }
}
return mode;
}