const clueDiv = document.querySelector('#clue');
console.log(clueDiv);

const url = 'https://jservice.xyz/api/random-clue';
const response = await fetch(url);

function createClueHtml(category, question, answer) {
  return `
    <div>
      <h2>${category}</h2>
      <p><b>Question</b>: ${question}</p>
      <p><b>Answer</b>:<font color="white"> ${answer}</font></p>
    </div>
  `;
}

if (response.ok) {
  console.log(response);

  const data = await response.json();
  console.log(data);

  const category = data.category.title;
  const question = data.question;
  const answer = data.answer;
  const html = createClueHtml(category, question, answer);
  console.log(html);

  clueDiv.innerHTML = html;
} else {
  console.error('Got an error in the response.')
}

// populate categories drop-down with entries from API
const categoriesURL = "https://jservice.xyz/api/categories";

const categoriesSelect = document.getElementById("categoryId");

console.log(categoriesSelect)

const categoriesResponse = await fetch(categoriesURL)
if (categoriesResponse.ok) {
  const data = await categoriesResponse.json();
  for (const category of data.categories.slice(0, 100)) {
    const option = document.createElement('option');
    option.innerHTML = category.title;
    option.value = category.id;
    categoriesSelect.appendChild(option);
  }
}



// add event listener to form to submit data
const clueForm = document.getElementById("create-clue-form")

async function onFormSubmit(event) {
  event.preventDefault();

  const formData = new FormData(clueForm);
  const dataObject = Object.fromEntries(formData);
  console.log(dataObject);

  const newClueURL = "https://jservice.xyz/api/clues";

  const fetchOptions = {
    method: "post",
    body: JSON.stringify(dataObject),
    headers: {
      'Content-Type': 'application/json'
    }
  }

  const newClueResponse = await fetch(newClueURL, fetchOptions);
  if (newClueResponse.ok) {
    const data = await newClueResponse.json();

    const category = data.category.title;
    const question = data.question;
    const answer = data.answer;
    const html = createClueHtml(category, question, answer);

    clueDiv.innerHTML = html;
  }
}

clueForm.addEventListener("submit", onFormSubmit)
